import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//Importar rutas
import { ROUTING, APP_ROUTING_PROVIDERS } from './app.routing';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SliderComponent } from './components/slider/slider.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';
import { FilterComponent } from './components/filter/filter.component';
import { BodyTwoComponent } from './components/body-two/body-two.component';
import { BodyThreeComponent } from './components/body-three/body-three.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SliderComponent,
    BodyComponent,
    FooterComponent,
    FilterComponent,
    BodyTwoComponent,
    BodyThreeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ROUTING
  ],
  providers: [APP_ROUTING_PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule { }
