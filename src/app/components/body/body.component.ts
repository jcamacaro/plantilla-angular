import { Component, OnInit } from '@angular/core';
//cargar servicio
import { PeticionesService } from '../../services/peticiones.service';
//Modelo de usuario
import { Usuario } from '../../models/usuario';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css'],
  providers: [PeticionesService]
})
export class BodyComponent implements OnInit {

	public divFilter:boolean = false;
    public usuarios:Usuario;
    public moreUsuarios:Array<Usuario> = [];
    public page:any;

  	constructor( private _peticionesService:PeticionesService ) { 
        this.usuarios = new Usuario (, '', '', '');
        this.page = 1;
    }

  	ngOnInit() {
        this.filterStandar(this.page);
  	}

    filterStandar(page){
        this.usuarios = false;
        this._peticionesService.getUser(page).subscribe(
            result => {
                this.usuarios = result.data;
                console.log(this.usuarios);
            },
            error => {
                console.log(<any>error);
            }
        );
    }


  	filter(value){
  		this.divFilter = value;
  	}

}
