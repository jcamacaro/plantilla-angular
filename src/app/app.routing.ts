//Importar modulos del router de angular
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importar componentes
import { BodyComponent } from './components/body/body.component';
import { BodyTwoComponent } from './components/body-two/body-two.component';
import { BodyThreeComponent } from './components/body-three/body-three.component';

const APP_ROUTES:Routes = [
	{path:'', component: BodyComponent},
	{path:'body', component: BodyComponent},
	{path:'bodytwo', component: BodyTwoComponent},
	{path:'bodythree', component: BodyThreeComponent},
	{path: '**', component: BodyComponent}
];

//Exportar el modulo del router
export const APP_ROUTING_PROVIDERS:any[] = [];
export const ROUTING:ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
